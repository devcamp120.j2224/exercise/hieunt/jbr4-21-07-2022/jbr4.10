package com.jbr410.circlecylinderapi;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class controller {
    @CrossOrigin
    @GetMapping("/circle-area")
    public double getCircleArea(@RequestParam(required = true) double radius) {
        Circle circle = new Circle(radius);
        return circle.getArea();
    }

    @CrossOrigin
    @GetMapping("/cylinder-volume")
    public double getCylinderVolume(
        @RequestParam(required = true) double radius,
        @RequestParam(required = true) double height
    ) {
        Cylinder cylinder = new Cylinder(radius, height);
        return cylinder.getVolume();
    }
}
